const express = require('express'); 
const FTPServer = require('ftp-srv'); 
const PORT = 8080;
const HOST = '0.0.0.0'; 

const app = express();
app.get('/', (req, res) => {
  res.send('Hello World');
});

app.listen(PORT, HOST, () => {
  console.log(`Running on http://${HOST}:${PORT}`);
}); 

const server = new FTPServer({
  url: `ftp://${HOST}:21`, 
  pasv_url: `ftp://${HOST}:3000`, 
  pasv_min: 3000,  
  pasv_max: 3100,  
  anonymous: true, // Allow anonymous access
  greeting: 'FTP server!', 
}); 

// Start the server
server.listen()
  .then(() => {
    console.log('FTP server started');
  })
  .catch((error) => {
    console.error('Error starting FTP server:', error);
  });